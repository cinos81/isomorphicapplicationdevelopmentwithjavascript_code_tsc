/*
 * Learning Isomorphic Web Application Development
 * Copyright © 2016 Konstantin Tarkus, Packt Publishing
 */

import * as React from "react";
import { Header } from "../Header/Header";
import * as s from './Layout.scss';

interface Props {
  children: JSX.Element;
  hero: JSX.Element;
}

export const Layout = (props: Props) => {
  return (
    <div className={s.root}>
      <Header>{props.hero}</Header>
      <main>{props.children}</main>
      <footer>
        <span>© Company Name</span>
      </footer>
    </div>
  );
};
