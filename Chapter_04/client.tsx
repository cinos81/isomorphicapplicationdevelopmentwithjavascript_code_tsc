import * as React from "react";
import * as ReactDOM from "react-dom";
import { router } from "./core/Router";

const run = () => {
  const component: JSX.Element = router.match(window.location.pathname);

  ReactDOM.hydrate(component, document.getElementById("app"));
};

const loadedStates = ["complete", "loaded", "interactive"];

if (loadedStates.includes(document.readyState) && document.body) {
  run();
} else {
  window.addEventListener("DOMContentLoaded", run, false);
}
