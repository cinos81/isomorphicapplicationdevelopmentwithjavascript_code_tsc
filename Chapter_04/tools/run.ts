/*
 * Learning Isomorphic Web Application Development
 * Copyright © 2016 Konstantin Tarkus, Packt Publishing
 */

function format(time: Date) {
  return time.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, '$1');
}

export function run(task: any, options?: any) {
  const start = new Date();
  console.log(
    `[${format(start)}] Starting '${task.name}${options ? `(${JSON.stringify(options, null, 2)})` : ''}'...`
  );
  return task(options).then(() => {
    const end = new Date();
    const time = end.getTime() - start.getTime();
    console.log(
      `[${format(end)}] Finished '${task.name}${options ? `(${JSON.stringify(options, null, 2)})` : ''}' after ${time} ms`
    );
  });
}

// TODO: process.mainModule ?? , require.cache??
if (process.mainModule.children.length === 0 && process.argv.length > 2) {
  delete require.cache[__filename];
  import(`./${process.argv[2]}.ts`).then(module => {
	  for (const name in module) {
		  run(module[name]).catch((err: Error) => {
			  console.error(err);
		  });
	  }
  });
}
