import * as path from "path";
import * as cp from "child_process";
import webpackConfig from "./webpack.config";
import { Configuration } from "webpack";
import { ChildProcess } from "child_process";

const RUNNING_REGEXP = /Server is listening at http:\/\/(.*?)\//;

let server: ChildProcess;
const { output } = (webpackConfig as Configuration[]).find(
  (x: Configuration) => x.target === "node"
) as Configuration;
const serverPath = path.join(output.path, output.filename);

export const runServer = (cb: (x?: any, path?: string) => void) => {
  const onStdOut = (data: any) => {
    const match = data.toString("utf8").match(RUNNING_REGEXP);
    process.stdout.write(data);

    if (match) {
      server.stdout.removeListener("data", onStdOut);
      server.stdout.on("data", data => process.stdout.write(data));
    }
    if (cb) {
      cb(null, match[1]);
    }
  };

  if (server) {
    server.kill("SIGTERM");
  }

  server = cp.spawn("node", [serverPath], {
    env: {
      ...process.env,
      NODE_ENV: "development"
    }
  });

  server.stdout.on("data", onStdOut);
  server.stderr.on("data", (data: any) => process.stderr.write(data));
};
