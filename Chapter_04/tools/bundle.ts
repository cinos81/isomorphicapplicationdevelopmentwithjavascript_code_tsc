import { BuildOptions } from "./build";
import webpackConfig from "./webpack.config";
import { Configuration } from "webpack";
import * as webpack from "webpack";

export const bundle = async (param: BuildOptions) => {
  // TODO: 웹팩으로 코드 번들링
  return new Promise((resolve, reject) => {
    // let bundlerRunCount: number = 0;
    const bundler = webpack(webpackConfig as Configuration[]);
    const cb: any = (err: Error, stats: string) => {
      if (err) {
        reject(err);
      }
      console.log(stats.toString());
      // if (++bundlerRunCount === (param.watch ? webpackConfig.length : 1)) {
      // 	resolve();
      // }
      resolve();
    };
    if (param.watch) {
      bundler.watch({ aggregateTimeout: 200 }, cb);
    } else {
      bundler.run(cb);
    }
  });
};
