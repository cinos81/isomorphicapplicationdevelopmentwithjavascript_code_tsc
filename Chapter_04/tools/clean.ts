/*
 * Learning Isomorphic Web Application Development
 * Copyright © 2016 Konstantin Tarkus, Packt Publishing
 */

import * as del from "del";

export async function clean() {
  await del(["build/*", "!build/.git"], { dot: true });
}
