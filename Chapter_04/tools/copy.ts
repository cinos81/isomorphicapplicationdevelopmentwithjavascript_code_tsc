import * as Promisify from 'bluebird';

export const copy = async() => {
	// TODO: 정적 파일의 복사
	const ncp = Promisify.promisify(require('ncp')) as (from: string, to: string, cb: Function) => void;
	await ncp('public', 'build/public', (err: Error) => console.log(err));
	await ncp('package.json', 'build/package.json', (err: Error) => console.log(err));
};