import { run } from "./run";
import { clean } from "./clean";
import * as webpack from "webpack";
import webpackConfig from "./webpack.config";
import { MultiCompiler, Configuration, Compiler } from "webpack";
import { runServer } from "./runServer";
import * as webpackDevMiddleware from "webpack-dev-middleware";
import * as webpackHotMiddleware from "webpack-hot-middleware";
import * as Browsersync from "browser-sync";
import * as path from "path";
import { MiddlewareHandler } from "browser-sync";
import * as http from "http";

export const start = async () => {
  await run(clean);
  await new Promise(resolve => {
    webpackConfig
      .filter((config: Configuration) => config.target !== "node")
      .forEach((config: Configuration) => {
        config.entry = [
          config.entry as string,
          "webpack-dev-middleware/client"
        ];
        config.plugins.push(new webpack.HotModuleReplacementPlugin());
        config.plugins.push(new webpack.NoEmitOnErrorsPlugin());
      });

    const bundler: MultiCompiler = webpack(webpackConfig as Configuration[]);
    const middleware: MiddlewareHandler[] = [
      (
        req: http.IncomingMessage,
        res: http.ServerResponse,
        next: () => void
      ) => {
        webpackDevMiddleware(bundler, {
          publicPath: ".",
          stats: webpackConfig[0].stats
        });
        next();
      },
      (
        req: http.IncomingMessage,
        res: http.ServerResponse,
        next: () => void
      ) => {
        bundler.compilers
          .filter(compiler => compiler.options.target !== "node")
          .map(compiler => webpackHotMiddleware(compiler));
        next();
      }
    ];
    let handleServerBundleComplete = (cb?: any) => {
      console.log("음?");
      runServer((err, host) => {
        if (!err) {
          const bs = Browsersync.create();
          bs.init(
            {
              proxy: { target: host },
              middleware,
              serveStatic: [
                path.join(__dirname, "../public"),
                path.join(__dirname, "../node_modules/graphiql"),
                path.join(__dirname, "../node_modules/react/dist"),
                path.join(__dirname, "../node_modules/react-dom/dist"),
                path.join(__dirname, "../node_modules/whatwg-fetch")
              ]
            },
            resolve
          );
          handleServerBundleComplete = runServer;
        }
      });
    };

    // bundler.compiler[0].hooks
    // bundler.plugin("done", () => handleServerBundleComplete());
    // compiler.hooks.someHook.tap(plugin, cb)
	  console.log("???");
    bundler.compilers
      .find((compiler: Compiler) => compiler.options.target !== "node")
      .hooks.done.tap("MyPlugin", params => {
        console.log("Synchronously tapping the compile hook.");
      });
    // .plugin("done", () => handleServerBundleComplete());
  });
};
