import { run } from "./run";
import { clean } from "./clean";
import { copy } from "./copy";
import { bundle } from "./bundle";

export interface BuildOptions {
  watch?: boolean;
}

export const build = async (options: BuildOptions = { watch: false }) => {
  await run(clean);
  await run(copy);
  await run(bundle, options);
};
