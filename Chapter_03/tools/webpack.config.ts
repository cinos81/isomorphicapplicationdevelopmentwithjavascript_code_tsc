/*
 * Learning Isomorphic Web Application Development
 * Copyright © 2016 Konstantin Tarkus - Tomas Alabes, Packt Publishing
 */

import * as path from 'path';
import * as fs from 'fs';
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const common = {

  // stats: {
  //   colors: true,
  //   chunks: false
  // },
  // resolve: {
		// alias: {
		// 	components: path.join(__dirname, '../', 'components')
  // },
	resolve: {
		extensions: ['.ts', '.tsx', '.js', '.json'],
		// modules: [
		// 	"node_modules",
		// 	path.resolve(__dirname, '../', 'components')
		// ],
		// alias: {
		// 	'components': path.join(__dirname, '../', 'components')
		// }
	},

	mode: 'development',

  module: {
    rules: [
      {
        test: /\.tsx$/,
        include: [
	        path.join(__dirname, '../'),
          path.join(__dirname, '../api'),
          path.join(__dirname, '../components'),
          path.join(__dirname, '../core'),
          path.join(__dirname, '../data'),
          path.join(__dirname, '../routes'),
          path.join(__dirname, '../client.js'),
          path.join(__dirname, '../server.js')
        ],
        loader: 'ts-loader'
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/,
        loader: 'url-loader?limit=10000&name=[path][name].[ext]'
      },
      {
        test: /\.(eot|ttf|wav|mp3)$/,
        loader: 'file-loader&name=[path][name].[ext]'
      },
      {
        test: /\.scss$/,
        include: [
          path.join(__dirname, '../components')
        ],
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                modules: true,
                localIdentName: '[name]_[local]_[hash:base64:3]'
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: (loader: any) => [
                  require('postcss-import')({ root: loader.resourcePath }),
                  require('precss')(),
                  require('autoprefixer')()
                ]
              }
            }
          ]
        })
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('style.css'),
  ]

};

const client = {
	...common,
	entry: path.join(__dirname, '../client.tsx'),

	output: {
		publicPath: '/',
		path: path.join(__dirname, '../build/public'),
		filename: 'client.js'
	}
};

const server = {
	...common,
	entry: path.join(__dirname, '../server.tsx'),

	output: {
		path: path.join(__dirname, '../build'),
		filename: 'server.js',
		libraryTarget: 'commonjs2'
	},

	target: 'node',

  // 혹은 nodeExternals를 사용해서 배제한다
	externals: [fs.readdirSync("node_modules")],

	node: {
		console: false,
		global: false,
		process: false,
		Buffer: false,
		__filename: false,
		__dirname: false
	}
};

// Remove `style-loader` from the server-side bundle configuration
// style-loader는 서버에서 동작시킬 수 없기 때문
// server.module.rules[3].use.splice(0, 1);
// server.module.rules[3].use.unshift({
//   loader: 'node-style-loader'
// });

export default [client, server];
