import { run } from './run';
import * as del from 'del';
import * as Promisify from 'bluebird';
import * as webpack from 'webpack';
import webpackConfig from './webpack.config';
import { Configuration } from 'webpack';

export interface BuildOptions {
	watch?: boolean;
}

const clean = async() => {
  // TODO: 결과물 디렉토리 초기화
	await del(['build/*', '!build/.git'], { dot: true });
};

const copy = async() => {
  // TODO: 정적 파일의 복사
  const ncp = Promisify.promisify(require('ncp')) as (from: string, to: string, cb: Function) => void;
  await ncp('public', 'build/public', (err: Error) => console.log(err));
  await ncp('package.json', 'build/package.json', (err: Error) => console.log(err));
};

const bundle = async(param: BuildOptions) => {
  // TODO: 웹팩으로 코드 번들링
	return new Promise((resolve, reject) => {
		// let bundlerRunCount: number = 0;
		const bundler = webpack(webpackConfig as Configuration[]);
		const cb: any = (err: Error, stats: string) => {
			if (err) {
				reject(err);
			}
			console.log(stats.toString());
			// if (++bundlerRunCount === (param.watch ? webpackConfig.length : 1)) {
			// 	resolve();
			// }
			resolve();
		};
		if (param.watch) {
			bundler.watch({ aggregateTimeout:200 }, cb);
		} else {
			bundler.run(cb);
		}
	});
};

export const build = async(options: BuildOptions = { watch: false }) => {
  await run(clean);
  await run(copy);
  await run(bundle, options);
};