/*
 * Learning Isomorphic Web Application Development
 * Copyright © 2016 Konstantin Tarkus, Packt Publishing
 */

import * as React from "react";
import { Header } from "../Header/Header";

interface Props {
  children: JSX.Element;
  hero: JSX.Element;
}

export const Layout = (props: Props) => {
  return (
    <div>
      <Header>{props.hero}</Header>
      <main>{props.children}</main>
      <footer>
        <span>© Company Name</span>
      </footer>
    </div>
  );
};
