/*
 * Learning Isomorphic Web Application Development
 * Copyright © 2016 Konstantin Tarkus, Packt Publishing
 */

import * as React from "react";

interface Props {
  children: JSX.Element;
}

export const Header = (props: Props) => {
  return (
    <header>
      <div>
        <span>My App</span>
        {!props.children && (
          <form>
            <input type="search" />
          </form>
        )}
        <div>
          <span>Username</span>
          <img src="#" />
        </div>
      </div>
      {props.children}
    </header>
  );
};
