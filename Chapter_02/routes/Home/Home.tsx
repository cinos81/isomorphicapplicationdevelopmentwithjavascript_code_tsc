/*
 * Learning Isomorphic Web Application Development
 * Copyright © 2016 Konstantin Tarkus, Packt Publishing
 */

import * as React from "react";
import { Layout } from "../../components/Layout/Layout";
import { Hero } from "./Hero";

export const path = "/";
export const action = () => (
  <Layout hero={<Hero />}>
    <Home />
  </Layout>
);

class Home extends React.Component {
  handleClick(event: any) {
    event.preventDefault();
    window.location = event.currentTarget.pathname;
  }

  render() {
    return (
      <div>
        <h2>Popular things to rent</h2>
        <div>
          <a href="/s/Tools" onClick={this.handleClick}>
            <span>Tools</span>
          </a>
          <a href="/s/Books" onClick={this.handleClick}>
            <span>Books</span>
          </a>
          ...
        </div>
      </div>
    );
  }
}
