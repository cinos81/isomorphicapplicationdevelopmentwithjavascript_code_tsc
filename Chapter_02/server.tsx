import * as express from 'express';
import * as core from "express-serve-static-core";
import * as React from 'react';
import * as path from 'path';
import * as ReactDOM from 'react-dom/server';
import { Html } from './components/Html';
import { router } from "./core/Router";

const server = express();
const port = 3000;
server.use(express.static(path.join(__dirname, 'public')));

server.get('*', (req: core.Request, res: core.Response) => {

	const component: JSX.Element = router.match(req.path);
	const body: string = ReactDOM.renderToString(component);
	const html: string = ReactDOM.renderToStaticMarkup(
		<Html title="MyApp" description="아이소모픽한 애플리케이션" body={body} />
	);
	res.send(`<!doctype html \n> ${html}`);
});

server.listen(port, async() => {
	console.log(`started at http://localhost:${port}`);
});