/*
 * Learning Isomorphic Web Application Development
 * Copyright © 2016 Konstantin Tarkus, Packt Publishing
 */

interface Route {
  action: () => JSX.Element;
  path: string;
}

const routes: Route[] = [
  require("../routes/Home"),
	require('../routes/NotFound')
];

export const router = {
  match(pathname: string): JSX.Element {
    const route = routes.find((x: Route) => x.path === pathname);

    if (route) {
      try {
        return route.action();
      } catch (err) {
        return routes.find((x: Route) => x.path === "/500").action();
      }
    } else {
      return routes.find((x: any) => x.path === "/404").action();
    }
  }
};
