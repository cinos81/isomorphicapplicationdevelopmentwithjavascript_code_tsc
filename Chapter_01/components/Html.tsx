/*
 * Learning Isomorphic Web Application Development
 * Copyright © 2016 Konstantin Tarkus, Packt Publishing
 */

import * as React from 'react';

interface Props { title: string, description: string , body: string }

export function Html(props: Props) {
	return (
		<html>
		<head>
			<meta charSet="utf-8" />
			<meta httpEquiv="x-ua-compatible" content="ie=edge" />
			<title>{props.title}</title>
			<meta name="description" content={props.description} />
			<meta name="viewport" content="width=device-width, initial-scale=1" />
			<script src="client.js" async />
		</head>
		<body>
		<div id="app" dangerouslySetInnerHTML={{__html: props.body}} />
		</body>
		</html>
	);
}
