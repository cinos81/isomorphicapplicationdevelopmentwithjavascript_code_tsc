
### 이 프로젝트를 만들며 깨달은 점

JSX 문법이 하나라도 들어간 파일은 반드시 확장자를 *.tsx로 해야만 한다.
안 그러면 정체불명의 에러와 마주하게 된다.

대부분의 익스프레스 타입은 아래의 네임스페이스에 있다.
```
import * as core from "express-serve-static-core";
```
물론 `@types/express` 설치는 필수

서버측 ReactDOM은 네임스페이스가 다르다
```
import * as ReactDOM from 'react-dom/server';
```

babel-node와 비슷한 환경을 원한다면 ts-node를 사용하자

tsconfig를 잘 활용하면 임포트 경로를 단축시킬 수 있다
https://decembersoft.com/posts/say-goodbye-to-relative-paths-in-typescript-imports/

웹팩에도 알리어스 기능이 있다.
