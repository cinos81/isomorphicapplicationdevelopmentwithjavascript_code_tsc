import * as React from 'react';
import * as ReactDOM from 'react-dom';
// from : //https://decembersoft.com/posts/say-goodbye-to-relative-paths-in-typescript-imports/
import { App } from './components/App';

const run = () => ReactDOM.hydrate(<App />, document.getElementById('app'));

const loadedStates = ['complete', 'loaded', 'interactive'];

// TODO: 폴리필 주입이 필요
if (loadedStates.includes(document.readyState) && document.body) {
	run();
} else {
	window.addEventListener('DOMContentLoaded', run, false);
}